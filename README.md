# YPFB-Refinacion Security Client

Java library as API to use Security services

## Getting Started

Clone this repo and open it as Gradle project
```
$ git clone git@bitbucket.org:Swissbytes/security-client.git
```

### Prerequisites

- Java 8
- Gradle
- IntelliJ IDE
  - [CheckStyle plugin](https://plugins.jetbrains.com/plugin/1065-checkstyle-idea)
  - [Set Google Java Styles](https://github.com/HPI-Information-Systems/Metanome/wiki/Installing-the-google-styleguide-settings-in-intellij-and-eclipse)


### Installing

A step by step series of examples that tell you have to get a development env running

Say what the step will be

```
$ cd security-client
$ gradle clean
$ gradle build
```

The result jar file will be in build/libs folder


## Running the tests

```
$ gradle test
```

### Get Test Coverage report

```
$ gradle test jacocoTestReport
```

Open results in `security-client/build/jacocoHtml/index.html`

## Workflow model

We adopted [Gitflow model](https://www.atlassian.com/git/tutorials/comparing-workflows/gitflow-workflow)

## Code Styling

We adopted [Google Java Style](https://google.github.io/styleguide/javaguide.html) 

## To install the library

```gradle
   repositories { 
        jcenter()
        maven { url "https://jitpack.io" }
   }
   dependencies {
         compile 'org.bitbucket.Swissbytes:security-client:develop-SNAPSHOT'
   }
```  

## Authors

* **Swissbytes Ltda** - *YPFB Refinacion Team*

