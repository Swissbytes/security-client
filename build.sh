#!/usr/bin/env bash

CURRENTDIR=$(pwd)
FLAG=$1

banner(){
echo "----------------------------------"
echo $1
echo "----------------------------------"
}

init() {
  banner "INIT "
  ./gradlew clean
}

test() {
  banner "TEST "
  cd $CURRENTDIR
  ./gradlew test jacocoTestReport
}

buildWar(){
  banner "BUILD"
  cd $CURRENTDIR
  ./gradlew build
}

install(){
  banner "BUILD"
  cd $CURRENTDIR
  ./gradlew install
}

sonarAnalysis() {
  if [ "$FLAG" == "--sonar" ]
  then
    banner "INIT SONAR ANALYSIS"
    ./gradlew sonarqube
  fi
}

# build steps
init && test && sonarAnalysis && buildWar && install
