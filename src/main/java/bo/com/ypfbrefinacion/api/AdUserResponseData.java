package bo.com.ypfbrefinacion.api;

/**
 * Active Directory User Response Data
 *
 * @author David Batista Taboada.
 * @since 4/2/2018
 */
public class AdUserResponseData {
  public Long id;
  public String displayName;
  public String loginName;
  public String dn;
  public String firstName;
  public String lastName;
  public String title;
  public String phone;
  public String picture;
  public String emailAddress;
  public Boolean accountIsActive;
  public String physicalDeliveryOfficeName;
  public String state;
  public String dateCreated;
  public String dateLastModified;
  public String city;
  public String company;
  public String department;
  public String immediateSuperior;
  public Boolean hasDelegated;
}
