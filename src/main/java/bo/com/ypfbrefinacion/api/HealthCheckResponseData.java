package bo.com.ypfbrefinacion.api;

import bo.com.ypfbrefinacion.api.enums.StatusType;

/**
 * Health Check Response Data.
 *
 * @author David Batista Taboada.
 * @since 4/3/2018
 */
public class HealthCheckResponseData {
  public Long id;
  public String name;
  public String version;
  public String url;
  public String manualUrl;
  public StatusType status;
  public int serviceStatus;
  public int databaseStatus;
}
