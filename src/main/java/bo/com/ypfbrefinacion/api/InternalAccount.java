package bo.com.ypfbrefinacion.api;

import com.fasterxml.jackson.annotation.JsonProperty;

public class InternalAccount {
  public Long id;
  public String name;
  public String userName;
}
