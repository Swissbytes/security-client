package bo.com.ypfbrefinacion.api;

/**
 * Login Permission Response Data.
 *
 * @author David Batista Taboada.
 * @since 1/31/2018
 */
public class LoginPermissionResponseData {
  public long id;
  public String key;
}
