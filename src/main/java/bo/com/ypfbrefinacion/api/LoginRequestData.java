package bo.com.ypfbrefinacion.api;

/**
 * Login Request Data.
 *
 * @author David Batista Taboada.
 * @since 1/31/2018
 */
public class LoginRequestData {
  public String user;
  public String terminal;
  public String domain;

  public LoginRequestData(String user, String terminal, String domain) {
    this.user = user;
    this.terminal = terminal;
    this.domain = domain;
  }
}
