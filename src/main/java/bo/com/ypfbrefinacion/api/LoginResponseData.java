package bo.com.ypfbrefinacion.api;

/**
 * Login Response Data.
 *
 * @author David Batista Taboada.
 * @since 1/31/2018
 */
public class LoginResponseData {
  public LoginUserResponseData user;
  public LoginPermissionResponseData[] permissions;
  public String authorization;
  public String department;
  public long[] roles;
  public Boolean hasDelegated;
  public Boolean hasResponsible;
  public LoginResponsibleResponseData responsible;
}
