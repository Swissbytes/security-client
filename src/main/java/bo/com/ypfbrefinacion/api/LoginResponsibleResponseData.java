package bo.com.ypfbrefinacion.api;

/**
 * Login Responsible Response Data.
 *
 * @author David Batista Taboada.
 * @since 1/31/2018
 */
public class LoginResponsibleResponseData {
  public long id;
  public String displayName;
  public String user;
  public String terminal;
  public String domain;
}
