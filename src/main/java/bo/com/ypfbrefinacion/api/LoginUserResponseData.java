package bo.com.ypfbrefinacion.api;

/**
 * Login User Response Data.
 *
 * @author David Batista Taboada.
 * @since 1/31/2018
 */
public class LoginUserResponseData {
  public Long id;
  public String name;
  public String avatar;
  public String department;
}
