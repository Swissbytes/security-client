package bo.com.ypfbrefinacion.api;

import bo.com.ypfbrefinacion.api.enums.StatusType;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;


/**
 * Organizational Unit Data
 *
 * @author David Batista Taboada.
 * @since 2/8/2018
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class OrganizationalUnitData {
  public long id;
  public String description;
  public String initials;
  public OrganizationalUnitData parent;
  public OrganizationalUnitData previous;
  public OrganizationalUnitData next;
  public StructureOrganizationalUnitData structureOrganizationalUnit;
  public PhisicalUnitData phisicalUnit;
  public LogicUnitData logicUnit;
  public StatusType status;
}
