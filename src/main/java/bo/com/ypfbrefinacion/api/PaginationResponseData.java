package bo.com.ypfbrefinacion.api;

import java.util.List;

/**
 * Pagination Response Data.
 *
 * @author David Batista Taboada.
 * @since 2/1/2018
 */
public class PaginationResponseData<T> {
  public long total;
  public long totalPage;
  public List<T> data;
}
