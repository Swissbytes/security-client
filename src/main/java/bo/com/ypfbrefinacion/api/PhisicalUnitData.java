package bo.com.ypfbrefinacion.api;

import bo.com.ypfbrefinacion.api.enums.StatusType;

/**
 * Phisical Unit Data
 *
 * @author David Batista Taboada.
 * @since 2/8/2018
 */
public class PhisicalUnitData {
  public long id;
  public String name;
  public StatusType status;
}
