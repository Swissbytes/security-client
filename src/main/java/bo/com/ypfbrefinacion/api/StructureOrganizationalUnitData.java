package bo.com.ypfbrefinacion.api;

import bo.com.ypfbrefinacion.api.enums.StatusType;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
public class StructureOrganizationalUnitData {
  public long id;
  public StatusType status;
  public String description;
}
