package bo.com.ypfbrefinacion.api;

import bo.com.ypfbrefinacion.api.enums.StatusType;

/**
 * User Response Data
 *
 * @author David Batista Taboada.
 * @since 2/1/2018
 */
public class UserResponseData {
  public long id;
  public String dn;
  public String createDate;
  public String lastChangeDate;
  public StatusType status;
  public String loginName;
  public String displayName;
  public String firstName;
  public String lastName;
  public String department;
  public String email;
}
