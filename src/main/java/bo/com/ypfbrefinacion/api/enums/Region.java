package bo.com.ypfbrefinacion.api.enums;

/**
 * Region Enum
 *
 * @author David Batista Taboada.
 * @since 2/23/2018
 */
public enum Region {
  SANTA_CRUZ, COCHABAMBA, LA_PAZ, WITHOUT_REGION
}
