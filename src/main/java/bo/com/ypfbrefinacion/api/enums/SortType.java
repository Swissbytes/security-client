package bo.com.ypfbrefinacion.api.enums;

/**
 * Sort Type Enum
 *
 * @author David Batista Taboada.
 * @since 2/22/2018
 */
public enum SortType {
  ASC("+"), DESC("-");

  private String value;

  SortType(String description) {
    this.value = description;
  }

  public String getValue() {
    return value;
  }
}
