package bo.com.ypfbrefinacion.api.enums;

import java.util.Arrays;
import java.util.Optional;

/**
 * Status Type enum
 *
 * @author David Batista Taboada.
 * @since 2/1/2018
 */
public enum StatusType {
  ENABLED("Item is enabled and visible"),
  DISABLED("Item is disabled but visible"),
  DELETED("Item is deleted and hidden");

  public String description;

  StatusType(String description) {
    this.description = description;
  }

  public static Optional<StatusType> getByName(String statusName) {
    return Arrays.stream(values())
        .filter(type -> type.name().equals(statusName))
        .findFirst();
  }


}
