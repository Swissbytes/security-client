package bo.com.ypfbrefinacion.api.securityPagination;

/**
 * SecurityPaginationPaging
 *
 * @author David Batista Taboada.
 * @since 2/1/2018
 */
public class SecurityPaginationPaging {
  public long totalRecords;
  public long pagesCount;
}
