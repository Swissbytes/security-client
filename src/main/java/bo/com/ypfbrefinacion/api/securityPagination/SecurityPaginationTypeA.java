package bo.com.ypfbrefinacion.api.securityPagination;

import bo.com.ypfbrefinacion.api.PaginationResponseData;
import java.util.List;

/**
 * SecurityPaginationTypeA
 *
 * @author David Batista Taboada.
 * @since 2/1/2018
 */
public class SecurityPaginationTypeA<T> {
  public int numberOfRows;
  public int pagesCount;
  public List<T> rows;

  public <T> PaginationResponseData getPaginationData(){
    PaginationResponseData<T> paginationResponseData = new PaginationResponseData<>();
    paginationResponseData.total = numberOfRows;
    paginationResponseData.totalPage = pagesCount;
    paginationResponseData.data = (List<T>) this.rows;
    return paginationResponseData;
  }
}
