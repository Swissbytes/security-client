package bo.com.ypfbrefinacion.api.securityPagination;

import bo.com.ypfbrefinacion.api.PaginationResponseData;
import java.util.List;

/**
 * SecurityPaginationTypeB
 *
 * @author David Batista Taboada.
 * @since 2/1/2018
 */
public class SecurityPaginationTypeB<T> {
  public SecurityPaginationPaging paging;
  public List<T> entries;

  public <T> PaginationResponseData getPaginationData(){
    PaginationResponseData<T> paginationResponseData = new PaginationResponseData<>();
    paginationResponseData.total = this.paging.totalRecords;
    paginationResponseData.totalPage = this.paging.pagesCount;
    paginationResponseData.data = (List<T>) this.entries;
    return paginationResponseData;
  }
}
