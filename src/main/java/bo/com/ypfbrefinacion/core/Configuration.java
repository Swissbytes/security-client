package bo.com.ypfbrefinacion.core;

import bo.com.ypfbrefinacion.exceptions.ConfigurationNotFoundException;

/**
 * Configuration Security Client
 *
 * @author David Batista Taboada.
 * @since 1/30/2018
 */
public class Configuration {
  private String serverUrl;
  private String publicToken;

  public Configuration(String serverUrl) throws ConfigurationNotFoundException {
    if (serverUrl.trim().isEmpty()){
      throw new ConfigurationNotFoundException("Server URL is empty.");
    }
    this.serverUrl = validateServerUrl(serverUrl);
    this.publicToken = "";
  }

  public Configuration(String serverUrl, String publicToken) throws ConfigurationNotFoundException {
    if (serverUrl.trim().isEmpty() || publicToken.trim().isEmpty()){
      throw new ConfigurationNotFoundException("Server URL or Public Token is empty.");
    }
    this.serverUrl = validateServerUrl(serverUrl.trim());
    this.publicToken = publicToken.trim();
  }

  public String getServerUrl() {
    return serverUrl;
  }

  public String getPublicToken() {
    return publicToken;
  }

  private String validateServerUrl(String serverUrl){
    return serverUrl.charAt(serverUrl.length() - 1) == '/' ? serverUrl : serverUrl + "/";
  }
}
