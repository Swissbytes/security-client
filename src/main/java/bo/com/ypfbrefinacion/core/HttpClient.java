package bo.com.ypfbrefinacion.core;

import bo.com.ypfbrefinacion.exceptions.ServerSecurityException;
import bo.com.ypfbrefinacion.exceptions.UnauthorizedClientException;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JavaType;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.mashape.unirest.http.HttpResponse;
import com.mashape.unirest.http.Unirest;
import com.mashape.unirest.http.exceptions.UnirestException;
import com.mashape.unirest.request.GetRequest;
import com.mashape.unirest.request.HttpRequestWithBody;
import java.io.IOException;
import java.util.List;
import java.util.Optional;
import org.apache.http.HttpStatus;

/**
 * Http Client
 *
 * @author David Batista Taboada.
 * @since 1/30/2018
 */
public final class HttpClient{
  public final Configuration configuration;

  public HttpClient(Configuration configuration) {
    this.configuration = configuration;
  }

  public HttpResponse<String> get(String path, String userToken) throws UnirestException {
    return Optional.ofNullable(userToken).filter(value -> !value.isEmpty())
        .map(value -> privateGet(path).header("Authorization", value))
        .orElseGet(
            () -> !configuration.getPublicToken().isEmpty()
                ? privateGet(path).header("Internal-Authorization", configuration.getPublicToken())
                : privateGet(path)
        ).asString();
  }

  public HttpResponse<String> get(String path) throws UnirestException {
    return !configuration.getPublicToken().isEmpty()
        ? privateGet(path).header("Internal-Authorization", configuration.getPublicToken()).asString()
        : privateGet(path).asString();
  }

  public HttpResponse<String> post(String path)
      throws UnirestException {
    return privatePost(path).asString();
  }

  public HttpResponse<String> post(String path, String userToken)
      throws UnirestException {
    return privatePost(path, userToken).asString();
  }

  public <E> HttpResponse<String> post(String path, E body)
      throws UnirestException, JsonProcessingException {
    return this.post(path, body, "");
  }

  public <E> HttpResponse<String> post(String path, E body, String userToken)
      throws UnirestException, JsonProcessingException {
    return this.privatePost(path, userToken)
        .header("Content-Type", "application/json")
        .body( new ObjectMapper().writeValueAsString(body))
        .asString();
  }

  public HttpResponse<String> put(String path, String userToken)
      throws UnirestException {
    return privatePut(path, userToken).asString();
  }

  public  HttpResponse<String> put(String path)
      throws UnirestException, JsonProcessingException {
    return this.put(path,  "");
  }

  private HttpRequestWithBody privatePost(String path, String userToken)
      throws UnirestException {
    return Optional.ofNullable(userToken).filter(value -> !value.isEmpty())
        .map(value -> privatePost(path).header("Authorization", value))
        .orElseGet(
            () -> !configuration.getPublicToken().isEmpty()
                ? privatePost(path).header("Internal-Authorization", configuration.getPublicToken())
                : privatePost(path)
        );
  }

  public <E> E parseJsonToObject(String value, Class<E> type) throws IOException {
    return new ObjectMapper().readValue(value, type);
  }

  public <E> List<E> parseJsonToListObject(String value, Class<E> type) throws IOException {
    ObjectMapper mapper = new ObjectMapper();
    JavaType listType = mapper.getTypeFactory().constructCollectionType(List.class, type);
    return mapper.readValue(value, listType);
  }

  public void validateResponse(long actualHttpCode, long expectedHttpCode)
      throws UnauthorizedClientException, ServerSecurityException {
    if (actualHttpCode == HttpStatus.SC_UNAUTHORIZED) {
      throw new UnauthorizedClientException("Unauthorized in security.");
    }

    if (actualHttpCode != expectedHttpCode) {
      throw new ServerSecurityException("Security Client server error.");
    }
  }

  private GetRequest privateGet(String path){
    return Unirest.get(this.getFullPath(path));
  }

  private HttpRequestWithBody privatePost(String path){
    return Unirest.post(this.getFullPath(path));
  }

  private HttpRequestWithBody privatePut(String path){
    return Unirest.put(this.getFullPath(path));
  }

  private HttpRequestWithBody privatePut(String path, String userToken)
      throws UnirestException {
    return Optional.ofNullable(userToken).filter(value -> !value.isEmpty())
        .map(value -> privatePut(path).header("Authorization", value))
        .orElseGet(
            () -> !configuration.getPublicToken().isEmpty()
                ? privatePut(path).header("Internal-Authorization", configuration.getPublicToken())
                : privatePut(path)
        );
  }

  private String getFullPath(String resourcePath) {
    return configuration.getServerUrl().concat(resourcePath);
  }

  public String hello() {
    return "hello world";
  }
}
