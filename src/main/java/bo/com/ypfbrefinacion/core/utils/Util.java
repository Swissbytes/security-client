package bo.com.ypfbrefinacion.core.utils;

/**
 * Util Methods.
 *
 * @author David Batista Taboada.
 * @since 2/23/2018
 */
public class Util {
  public static String formatQueryParamValue(String field, String value) {
    return String.format("%s=%s&", field, value);
  }
}
