package bo.com.ypfbrefinacion.exceptions;

/**
 * Configuration NotFound Exception
 *
 * @author David Batista Taboada.
 * @since 1/30/2018
 */
public class ConfigurationNotFoundException extends Exception {
  public ConfigurationNotFoundException(String message) {
    super(message);
  }
}
