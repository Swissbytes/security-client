package bo.com.ypfbrefinacion.exceptions;

/**
 * Security Client Exception.
 *
 * @author David Batista Taboada.
 * @since 1/31/2018
 */
public class SecurityClientException extends Exception{
  public SecurityClientException(String message) {
    super(message);
  }
}
