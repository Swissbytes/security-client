package bo.com.ypfbrefinacion.exceptions;

/**
 * Server Security Exception
 *
 * @author David Batista Taboada.
 * @since 2/1/2018
 */
public class ServerSecurityException extends Exception {
  public ServerSecurityException(String message) {
    super(message);
  }
}
