package bo.com.ypfbrefinacion.exceptions;

/**
 * Unauthorized Client Exception
 *
 * @author David Batista Taboada.
 * @since 1/31/2018
 */
public class UnauthorizedClientException extends Exception {
  public UnauthorizedClientException(String message) {
    super(message);
  }
}
