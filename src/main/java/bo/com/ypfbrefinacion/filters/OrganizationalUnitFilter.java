package bo.com.ypfbrefinacion.filters;

import bo.com.ypfbrefinacion.api.enums.StatusType;
import bo.com.ypfbrefinacion.core.utils.Util;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

/**
 * Organizational Unit Filter
 *
 * @author David Batista Taboada.
 * @since 2/22/2018
 */
public class OrganizationalUnitFilter extends PaginationAbstractFilter {
  public Optional<String> description = Optional.empty();
  public Optional<String> wildcard = Optional.empty();
  public Optional<Long> parentId = Optional.empty();
  public Optional<Long> structureOuId = Optional.empty();
  public Optional<StatusType> status = Optional.empty();
  public List<String> initials = new ArrayList<>(0);
  public List<Long> phisicalUnits = new ArrayList<>(0);
  public List<Long> logicUnits = new ArrayList<>(0);

  public OrganizationalUnitFilter() {
  }

  public String getQueryParams() {
    StringBuilder stringBuilder = new StringBuilder("?");

    stringBuilder.append(this.getPaginationQueryParams());

    description.ifPresent(value ->
        stringBuilder.append(Util.formatQueryParamValue("description", value)));

    wildcard.ifPresent(value ->
        stringBuilder.append(Util.formatQueryParamValue("wildcard", value)));

    parentId.ifPresent(value ->
        stringBuilder.append(Util.formatQueryParamValue("parentId", value.toString())));

    structureOuId.ifPresent(value ->
        stringBuilder.append(Util.formatQueryParamValue("structureOuId", value.toString())));

    status.ifPresent(value ->
        stringBuilder.append(Util.formatQueryParamValue("status", value.toString())));

    initials.forEach(value ->
        stringBuilder.append(Util.formatQueryParamValue("initials", value)));

    phisicalUnits.forEach(value ->
        stringBuilder.append(Util.formatQueryParamValue("phisicalUnits", value.toString())));

    logicUnits.forEach(value ->
        stringBuilder.append(Util.formatQueryParamValue("logicUnits", value.toString())));

    return stringBuilder.deleteCharAt(stringBuilder.length() - 1).toString();
  }
}
