package bo.com.ypfbrefinacion.filters;

import bo.com.ypfbrefinacion.api.enums.SortType;
import java.util.Optional;

/**
 * Pagination Abstract Filter.
 *
 * @author David Batista Taboada.
 * @since 2/8/2018
 */
public abstract class PaginationAbstractFilter {
  public Optional<Long> page = Optional.empty();
  public Optional<Long> perPage = Optional.empty();
  public Optional<SortType> fieldToSort = Optional.empty();

  public PaginationAbstractFilter() {
  }

  public String getPaginationQueryParams(){
    StringBuilder stringBuilder = new StringBuilder("");
    page.ifPresent(value -> stringBuilder.append(getValue("page", value.toString())));
    perPage.ifPresent(value -> stringBuilder.append(getValue("per_page", value.toString())));
    fieldToSort.ifPresent(value -> stringBuilder.append(getValue("sort", value.getValue())));

    return stringBuilder.toString();
  }

  private String getValue(String field, String value) {
    return String.format("%s=%s&", field, value);
  }
}
