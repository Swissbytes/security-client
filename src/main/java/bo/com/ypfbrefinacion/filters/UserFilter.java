package bo.com.ypfbrefinacion.filters;

import bo.com.ypfbrefinacion.api.enums.Region;
import bo.com.ypfbrefinacion.api.enums.StatusType;
import bo.com.ypfbrefinacion.core.utils.Util;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

/**
 * User Filter.
 *
 * @author David Batista Taboada.
 * @since 2/23/2018
 */
public class UserFilter extends PaginationAbstractFilter{
  public Optional<String> wildcard = Optional.empty();
  public Optional<String> dn = Optional.empty();
  public Optional<String> displayName = Optional.empty();
  public Optional<StatusType> status = Optional.empty();

  public List<Long> ids = new ArrayList<>(0);
  public List<String> loginNames =  new ArrayList<>(0);
  public List<String> emails = new ArrayList<>(0);
  public List<Region> regions = new ArrayList<>(0);
  public List<String> departments = new ArrayList<>(0);
  public List<Long> roles = new ArrayList<>(0);
  public List<String> permissions = new ArrayList<>(0);

  public UserFilter() {
  }

  public String getQueryParams() {
    StringBuilder stringBuilder = new StringBuilder("?");
    stringBuilder.append(this.getPaginationQueryParams());
    wildcard.ifPresent(value ->
        stringBuilder.append(Util.formatQueryParamValue("wildcard", value)));

    dn.ifPresent(value ->
        stringBuilder.append(Util.formatQueryParamValue("dn", value)));

    displayName.ifPresent(value ->
        stringBuilder.append(Util.formatQueryParamValue("dn", value)));

    status.ifPresent(value ->
        stringBuilder.append(Util.formatQueryParamValue("status",value.toString())));

    ids.forEach(value ->
        stringBuilder.append(Util.formatQueryParamValue("id", value.toString())));

    loginNames.forEach(value ->
        stringBuilder.append(Util.formatQueryParamValue("loginName", value)));

    emails.forEach(value ->
        stringBuilder.append(Util.formatQueryParamValue("email", value)));

    regions.forEach(value ->
        stringBuilder.append(Util.formatQueryParamValue("region", value.toString())));

    departments.forEach(value ->
        stringBuilder.append(Util.formatQueryParamValue("department", value)));

    roles.forEach(value ->
        stringBuilder.append(Util.formatQueryParamValue("rol", value.toString())));

    permissions.forEach(value ->
        stringBuilder.append(Util.formatQueryParamValue("permission", value)));

    return stringBuilder.deleteCharAt(stringBuilder.length() - 1).toString();
  }
}
