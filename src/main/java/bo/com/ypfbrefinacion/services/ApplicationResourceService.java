package bo.com.ypfbrefinacion.services;

import bo.com.ypfbrefinacion.exceptions.SecurityClientException;
import bo.com.ypfbrefinacion.exceptions.ServerSecurityException;
import bo.com.ypfbrefinacion.exceptions.UnauthorizedClientException;

/**
 * Application Resource Service.
 *
 * @author David Batista Taboada.
 * @since 3/28/2018
 */
public interface ApplicationResourceService {
  void disableApplication(long applicationId) throws SecurityClientException, UnauthorizedClientException, ServerSecurityException;
}
