package bo.com.ypfbrefinacion.services;

import bo.com.ypfbrefinacion.api.InternalAccount;
import bo.com.ypfbrefinacion.exceptions.SecurityClientException;
import java.util.List;
import java.util.Optional;

public interface AuthenticationResourceService {
  Optional<InternalAccount> validateToken(String token) throws SecurityClientException;
  boolean hasPermision(String token, List<String> permissions) throws SecurityClientException;
}
