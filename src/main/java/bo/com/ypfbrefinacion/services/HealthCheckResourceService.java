package bo.com.ypfbrefinacion.services;

import bo.com.ypfbrefinacion.api.HealthCheckResponseData;
import bo.com.ypfbrefinacion.exceptions.SecurityClientException;
import bo.com.ypfbrefinacion.exceptions.ServerSecurityException;
import bo.com.ypfbrefinacion.exceptions.UnauthorizedClientException;
import java.util.List;

/**
 * Health Check Service.
 *
 * @author David Batista Taboada.
 * @since 4/3/2018
 */
public interface HealthCheckResourceService {
  List<HealthCheckResponseData> getHealthCheckApplications() throws UnauthorizedClientException, ServerSecurityException, SecurityClientException;
}
