package bo.com.ypfbrefinacion.services;

import bo.com.ypfbrefinacion.api.OrganizationalUnitData;
import bo.com.ypfbrefinacion.exceptions.SecurityClientException;
import bo.com.ypfbrefinacion.exceptions.ServerSecurityException;
import bo.com.ypfbrefinacion.exceptions.UnauthorizedClientException;
import bo.com.ypfbrefinacion.filters.OrganizationalUnitFilter;
import java.util.List;

/**
 * Organizational Unit Resource Service.
 *
 * @author David Batista Taboada.
 * @since 2/2/2018
 */
public interface OrganizationalUnitResourceService {
  OrganizationalUnitData getById(Long organizationalUnitId) throws UnauthorizedClientException, ServerSecurityException, SecurityClientException;
  List<OrganizationalUnitData> getCurrents(OrganizationalUnitFilter filter) throws SecurityClientException, UnauthorizedClientException, ServerSecurityException;
}
