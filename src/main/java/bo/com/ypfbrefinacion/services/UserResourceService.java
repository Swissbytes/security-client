package bo.com.ypfbrefinacion.services;

import bo.com.ypfbrefinacion.api.AdUserResponseData;
import bo.com.ypfbrefinacion.api.LoginRequestData;
import bo.com.ypfbrefinacion.api.LoginResponseData;
import bo.com.ypfbrefinacion.api.PaginationResponseData;
import bo.com.ypfbrefinacion.api.UserResponseData;
import bo.com.ypfbrefinacion.exceptions.SecurityClientException;
import bo.com.ypfbrefinacion.exceptions.ServerSecurityException;
import bo.com.ypfbrefinacion.exceptions.UnauthorizedClientException;
import bo.com.ypfbrefinacion.filters.UserFilter;
import java.nio.file.attribute.UserPrincipalNotFoundException;
import java.util.List;

/**
 * User Resource Service.
 *
 * @author David Batista Taboada.
 * @since 1/31/2018
 */
public interface UserResourceService {
  LoginResponseData login(LoginRequestData loginRequestData) throws
      UserPrincipalNotFoundException, SecurityClientException, UnauthorizedClientException,
      ServerSecurityException;

  PaginationResponseData<UserResponseData> getUsersSacSolvers() throws SecurityClientException,
      UnauthorizedClientException, ServerSecurityException;

  PaginationResponseData<UserResponseData> getUsersSacResponsible() throws
      UnauthorizedClientException,
      ServerSecurityException, SecurityClientException;

  List<UserResponseData> getSacResponsibleUsers()
      throws UnauthorizedClientException, ServerSecurityException, SecurityClientException;

  PaginationResponseData<UserResponseData> getUsersSacManagers() throws UnauthorizedClientException,
      ServerSecurityException, SecurityClientException;

  List<UserResponseData> getSacManagerUsers() throws
      UnauthorizedClientException, ServerSecurityException, SecurityClientException;

  UserResponseData getUserManagerById(long userId) throws UnauthorizedClientException,
      ServerSecurityException, SecurityClientException;

  PaginationResponseData<UserResponseData> getUsersAptAuditors() throws UnauthorizedClientException,
      ServerSecurityException, SecurityClientException;

  PaginationResponseData<UserResponseData> getAllUsers() throws UnauthorizedClientException,
      ServerSecurityException, SecurityClientException;

  List<UserResponseData> getUsersByFilter(UserFilter userFilter) throws
      UnauthorizedClientException, ServerSecurityException, SecurityClientException;

  AdUserResponseData getActiveDirectyUserById(Long id) throws UnauthorizedClientException,
      ServerSecurityException, SecurityClientException;
}
