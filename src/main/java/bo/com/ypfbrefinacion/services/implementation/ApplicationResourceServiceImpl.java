package bo.com.ypfbrefinacion.services.implementation;

import bo.com.ypfbrefinacion.core.Configuration;
import bo.com.ypfbrefinacion.core.HttpClient;
import bo.com.ypfbrefinacion.exceptions.SecurityClientException;
import bo.com.ypfbrefinacion.exceptions.ServerSecurityException;
import bo.com.ypfbrefinacion.exceptions.UnauthorizedClientException;
import bo.com.ypfbrefinacion.services.ApplicationResourceService;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.mashape.unirest.http.HttpResponse;
import com.mashape.unirest.http.exceptions.UnirestException;
import org.apache.http.HttpStatus;

/**
 * Application Resource Service Implementation
 *
 * @author David Batista Taboada.
 * @since 3/28/2018
 */
public class ApplicationResourceServiceImpl implements ApplicationResourceService {
  private final HttpClient httpClient;

  public ApplicationResourceServiceImpl(Configuration configuration) {
    this.httpClient = new HttpClient(configuration);
  }

  @Override
  public void disableApplication(long applicationId) throws SecurityClientException, UnauthorizedClientException, ServerSecurityException {
    try {
      HttpResponse<String> httpResponse = httpClient.put(
          String.format("applications/%d/disable", applicationId)
      );
      httpClient.validateResponse(httpResponse.getStatus(), HttpStatus.SC_ACCEPTED);
    } catch (JsonProcessingException | UnirestException e) {
      throw new SecurityClientException(e.getMessage());
    }
  }
}
