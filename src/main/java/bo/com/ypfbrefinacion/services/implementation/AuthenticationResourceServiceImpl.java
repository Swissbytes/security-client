package bo.com.ypfbrefinacion.services.implementation;

import bo.com.ypfbrefinacion.api.InternalAccount;
import bo.com.ypfbrefinacion.core.Configuration;
import bo.com.ypfbrefinacion.core.HttpClient;
import bo.com.ypfbrefinacion.exceptions.SecurityClientException;
import bo.com.ypfbrefinacion.services.AuthenticationResourceService;
import com.mashape.unirest.http.HttpResponse;
import com.mashape.unirest.http.exceptions.UnirestException;
import java.io.IOException;
import java.util.List;
import java.util.Optional;
import org.apache.http.HttpStatus;

public class AuthenticationResourceServiceImpl implements AuthenticationResourceService {

  private final HttpClient httpClient;

  public AuthenticationResourceServiceImpl(Configuration configuration) {
    this.httpClient = new HttpClient(configuration);
  }

  @Override
  public Optional<InternalAccount> validateToken(String token) throws SecurityClientException {
    try {
      HttpResponse<String> httpResponse = httpClient.post(String.format("authentication?token=%s", token), null);
      return httpResponse.getStatus() == HttpStatus.SC_UNAUTHORIZED
          ? Optional.empty()
          : Optional.of(
              httpClient.parseJsonToObject(httpResponse.getBody(), InternalAccount.class));
    } catch (UnirestException | IOException e) {
      throw new SecurityClientException(e.getMessage());
    }
  }

  @Override
  public boolean hasPermision(String token, List<String> permissions) throws SecurityClientException {
    try {
      if (permissions.isEmpty()) {
        return false;
      }
      StringBuilder stringBuilder = new StringBuilder();
      permissions.forEach(value -> {
        stringBuilder.append(String.format("&permission=%s", value));
      });
      HttpResponse<String> httpResponse = httpClient.post(
          String.format("authentication/has_permission?token=%s%s", token, stringBuilder.toString()), null);
      return httpResponse.getStatus() == HttpStatus.SC_OK;
    } catch (UnirestException e) {
      throw new SecurityClientException(e.getMessage());
    }
  }
}
