package bo.com.ypfbrefinacion.services.implementation;

import bo.com.ypfbrefinacion.api.HealthCheckResponseData;
import bo.com.ypfbrefinacion.core.Configuration;
import bo.com.ypfbrefinacion.core.HttpClient;
import bo.com.ypfbrefinacion.exceptions.SecurityClientException;
import bo.com.ypfbrefinacion.exceptions.ServerSecurityException;
import bo.com.ypfbrefinacion.exceptions.UnauthorizedClientException;
import bo.com.ypfbrefinacion.services.HealthCheckResourceService;
import com.mashape.unirest.http.HttpResponse;
import com.mashape.unirest.http.exceptions.UnirestException;
import java.io.IOException;
import java.util.List;
import org.apache.http.HttpStatus;

/**
 * Health Check Service Implementation
 *
 * @author David Batista Taboada.
 * @since 4/3/2018
 */
public class HealthCheckResourceServiceImpl implements HealthCheckResourceService {
  public final HttpClient httpClient;

  public HealthCheckResourceServiceImpl(Configuration configuration) {
    this.httpClient = new HttpClient(configuration);
  }

  @Override
  public List<HealthCheckResponseData> getHealthCheckApplications()
      throws UnauthorizedClientException, ServerSecurityException, SecurityClientException {
    try {
      HttpResponse<String> httpResponse = httpClient.get("healthCheck");
      httpClient.validateResponse(httpResponse.getStatus(), HttpStatus.SC_OK);
      return httpClient.parseJsonToListObject(httpResponse.getBody(), HealthCheckResponseData.class);
    } catch (UnirestException | IOException e) {
      throw new SecurityClientException(e.getMessage());
    }
  }
}
