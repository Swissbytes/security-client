package bo.com.ypfbrefinacion.services.implementation;

import bo.com.ypfbrefinacion.api.OrganizationalUnitData;
import bo.com.ypfbrefinacion.core.Configuration;
import bo.com.ypfbrefinacion.core.HttpClient;
import bo.com.ypfbrefinacion.exceptions.SecurityClientException;
import bo.com.ypfbrefinacion.exceptions.ServerSecurityException;
import bo.com.ypfbrefinacion.exceptions.UnauthorizedClientException;
import bo.com.ypfbrefinacion.filters.OrganizationalUnitFilter;
import bo.com.ypfbrefinacion.services.OrganizationalUnitResourceService;
import com.mashape.unirest.http.HttpResponse;
import com.mashape.unirest.http.exceptions.UnirestException;
import java.io.IOException;
import java.util.List;
import org.apache.http.HttpStatus;

/**
 * Organizational Unit Resource Service Implementation.
 *
 * @author David Batista Taboada.
 * @since 2/22/2018
 */
public class OrganizationalUnitResourceServiceImpl implements OrganizationalUnitResourceService {

  private final HttpClient httpClient;

  public OrganizationalUnitResourceServiceImpl(Configuration configuration) {
    this.httpClient = new HttpClient(configuration);
  }

  @Override
  public OrganizationalUnitData getById(Long organizationalUnitId)
      throws UnauthorizedClientException, ServerSecurityException, SecurityClientException {
    try {
      HttpResponse<String> httpResponse = httpClient.get(
          String.format("organizational_units/%d", organizationalUnitId)
      );
      httpClient.validateResponse(httpResponse.getStatus(), HttpStatus.SC_OK);
      return httpClient.parseJsonToObject(httpResponse.getBody(), OrganizationalUnitData.class);
    } catch (UnirestException | IOException e) {
      throw new SecurityClientException(e.getMessage());
    }
  }

  @Override
  public List<OrganizationalUnitData> getCurrents(OrganizationalUnitFilter filter)
      throws SecurityClientException, UnauthorizedClientException, ServerSecurityException {
    try {

      HttpResponse<String> httpResponse = httpClient.get(
          String.format("organizational_units/current/filter%s", filter.getQueryParams())
      );
      httpClient.validateResponse(httpResponse.getStatus(), HttpStatus.SC_OK);
      return httpClient.parseJsonToListObject(httpResponse.getBody(), OrganizationalUnitData.class);
    } catch (UnirestException | IOException e) {
      throw new SecurityClientException(e.getMessage());
    }
  }
}
