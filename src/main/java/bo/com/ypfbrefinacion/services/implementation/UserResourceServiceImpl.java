package bo.com.ypfbrefinacion.services.implementation;

import bo.com.ypfbrefinacion.api.AdUserResponseData;
import bo.com.ypfbrefinacion.api.LoginRequestData;
import bo.com.ypfbrefinacion.api.LoginResponseData;
import bo.com.ypfbrefinacion.api.PaginationResponseData;
import bo.com.ypfbrefinacion.api.UserResponseData;
import bo.com.ypfbrefinacion.api.securityPagination.SecurityPaginationTypeA;
import bo.com.ypfbrefinacion.core.Configuration;
import bo.com.ypfbrefinacion.core.HttpClient;
import bo.com.ypfbrefinacion.exceptions.SecurityClientException;
import bo.com.ypfbrefinacion.exceptions.ServerSecurityException;
import bo.com.ypfbrefinacion.exceptions.UnauthorizedClientException;
import bo.com.ypfbrefinacion.filters.UserFilter;
import bo.com.ypfbrefinacion.services.UserResourceService;
import com.mashape.unirest.http.HttpResponse;
import com.mashape.unirest.http.exceptions.UnirestException;
import java.io.IOException;
import java.nio.file.attribute.UserPrincipalNotFoundException;
import java.util.List;
import org.apache.http.HttpStatus;

/**
 * User Resource Service Implementation.
 *
 * @author David Batista Taboada.
 * @since 1/31/2018
 */
public class UserResourceServiceImpl implements UserResourceService {
  public final HttpClient httpClient;

  public UserResourceServiceImpl(Configuration configuration) {
    this.httpClient = new HttpClient(configuration);
  }

  @Override
  public LoginResponseData login(LoginRequestData loginRequestData)
      throws UserPrincipalNotFoundException, SecurityClientException,
              UnauthorizedClientException, ServerSecurityException {
    try {
      HttpResponse<String> httpResponse = httpClient.post("users/bootstraplogin",
          loginRequestData);
      httpClient.validateResponse(httpResponse.getStatus(), HttpStatus.SC_OK);
      if (httpResponse.getStatus() == HttpStatus.SC_NO_CONTENT) {
        throw new UserPrincipalNotFoundException("Unauthorized in security.");
      }
      return httpClient.parseJsonToObject(httpResponse.getBody(), LoginResponseData.class);
    } catch (UnirestException | IOException e) {
      throw new SecurityClientException(e.getMessage());
    }
  }

  @Override
  public PaginationResponseData<UserResponseData> getUsersSacSolvers()
      throws SecurityClientException, UnauthorizedClientException, ServerSecurityException {
    return getUsers("users/sac/solvers");
  }

  @Override
  public PaginationResponseData<UserResponseData> getUsersSacResponsible()
      throws UnauthorizedClientException, ServerSecurityException, SecurityClientException {
    return getUsers("users/sac/responsible");
  }

  @Override
  public List<UserResponseData> getSacResponsibleUsers()
      throws UnauthorizedClientException, ServerSecurityException, SecurityClientException {
    return getUserList("users/sac/responsible/list");
  }

  @Override
  public PaginationResponseData<UserResponseData> getUsersSacManagers() throws UnauthorizedClientException, ServerSecurityException, SecurityClientException {
    return getUsers("users/sac/managers");
  }

  @Override
  public List<UserResponseData> getSacManagerUsers() throws
      UnauthorizedClientException, ServerSecurityException, SecurityClientException {
    return getUserList("users/sac/managers/list");
  }

  @Override
  public UserResponseData getUserManagerById(long userId) throws UnauthorizedClientException, ServerSecurityException, SecurityClientException {
    try {
      HttpResponse<String> httpResponse = httpClient.get("users/manager/".concat(String.valueOf(userId)));
      httpClient.validateResponse(httpResponse.getStatus(), HttpStatus.SC_OK);
      return httpClient.parseJsonToObject(httpResponse.getBody(), UserResponseData.class);
    } catch (UnirestException | IOException e) {
      throw new SecurityClientException(e.getMessage());
    }
  }

  @Override
  public PaginationResponseData<UserResponseData> getUsersAptAuditors() throws UnauthorizedClientException, ServerSecurityException, SecurityClientException {
    return this.getUsers("users/apt/auditors");
  }

  @Override
  public PaginationResponseData<UserResponseData> getAllUsers() throws UnauthorizedClientException, ServerSecurityException, SecurityClientException {
    return this.getUsers("users/");
  }

  @Override
  public List<UserResponseData> getUsersByFilter(UserFilter userFilter) throws UnauthorizedClientException, ServerSecurityException, SecurityClientException {
    try {
      HttpResponse<String> httpResponse = httpClient.get(
          String.format("users/filter%s", userFilter.getQueryParams())
      );
      httpClient.validateResponse(httpResponse.getStatus(), HttpStatus.SC_OK);
      return httpClient.parseJsonToListObject(httpResponse.getBody(), UserResponseData.class);
    } catch (UnirestException | IOException e) {
      throw new SecurityClientException(e.getMessage());
    }
  }

  @Override
  public AdUserResponseData getActiveDirectyUserById(Long id)
      throws UnauthorizedClientException, ServerSecurityException, SecurityClientException {
    try {
      HttpResponse<String> httpResponse = httpClient.get("users/active_directory/".concat(String.valueOf(id)));
      httpClient.validateResponse(httpResponse.getStatus(), HttpStatus.SC_OK);
      return httpClient.parseJsonToObject(httpResponse.getBody(), AdUserResponseData.class);
    } catch (UnirestException | IOException e) {
      throw new SecurityClientException(e.getMessage());
    }
  }

  private PaginationResponseData<UserResponseData> getUsers(String path)
      throws UnauthorizedClientException, ServerSecurityException, SecurityClientException{
    try {
      HttpResponse<String> httpResponse = httpClient.get(path);
      httpClient.validateResponse(httpResponse.getStatus(), HttpStatus.SC_OK);
      return httpClient.parseJsonToObject(httpResponse.getBody(), SecurityPaginationTypeA.class).getPaginationData();
    } catch (UnirestException | IOException e) {
      throw new SecurityClientException(e.getMessage());
    }
  }

  private List<UserResponseData> getUserList(String path)
      throws UnauthorizedClientException, ServerSecurityException, SecurityClientException{
    try {
      HttpResponse<String> httpResponse = httpClient.get(path);
      httpClient.validateResponse(httpResponse.getStatus(), HttpStatus.SC_OK);
      return httpClient.parseJsonToListObject(httpResponse.getBody(), UserResponseData.class);
    } catch (UnirestException | IOException e) {
      throw new SecurityClientException(e.getMessage());
    }
  }
}
