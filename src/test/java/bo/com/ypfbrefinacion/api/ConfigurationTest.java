package bo.com.ypfbrefinacion.api;

import static org.junit.Assert.assertThat;

import bo.com.ypfbrefinacion.core.Configuration;
import bo.com.ypfbrefinacion.exceptions.ConfigurationNotFoundException;
import org.hamcrest.CoreMatchers;
import org.junit.Test;

/**
 * Configuration Test
 *
 * @author David Batista Taboada.
 * @since 1/30/2018
 */
public class ConfigurationTest {
  @Test
  public void configurationWithOnlyServerUrl() throws ConfigurationNotFoundException {
    assertThat(new Configuration("http://localhost:8080/security/").getServerUrl(),
        CoreMatchers.equalTo("http://localhost:8080/security/"));
  }

  @Test
  public void configurationWithOnlyServerUrlAndToken() throws ConfigurationNotFoundException {
    Configuration configuration = new Configuration("http://localhost:8080/security/","MY_TOKEN  ");
    assertThat(configuration.getServerUrl(),
        CoreMatchers.equalTo("http://localhost:8080/security/"));
    assertThat(configuration.getPublicToken(),
        CoreMatchers.equalTo("MY_TOKEN"));
  }

  @Test(expected = ConfigurationNotFoundException.class)
  public void configurationWithOnlyServerEmpty() throws ConfigurationNotFoundException {
    Configuration configuration = new Configuration("  ");
  }

  @Test(expected = ConfigurationNotFoundException.class)
  public void configurationWithOnlyServerEmptyAndTokenEmpty()
      throws ConfigurationNotFoundException {
    Configuration configuration = new Configuration("  ","   ");
  }
}