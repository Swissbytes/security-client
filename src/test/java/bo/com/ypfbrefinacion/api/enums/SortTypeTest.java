package bo.com.ypfbrefinacion.api.enums;

import static org.junit.Assert.*;

import org.junit.Test;

public class SortTypeTest {

  @Test
  public void testForCoverage()  {
    SortType.valueOf(SortType.ASC.toString());
  }

  @Test
  public void testGetValue()  {
    assertEquals("+", SortType.ASC.getValue());
    assertEquals("-", SortType.DESC.getValue());
  }

}
