package bo.com.ypfbrefinacion.api.enums;

import static org.junit.Assert.*;

import java.util.Optional;
import org.junit.Test;

public class StatusTypeTest {

  @Test
  public void testForCoverage() {
    // Just to get coverage on enums
    StatusType.valueOf(StatusType.DELETED.toString());
  }

  @Test
  public void testGetByExistingNameReturnsNoEmpty()  {
    assertTrue(StatusType.getByName("DELETED").isPresent());
    assertTrue(StatusType.getByName("DISABLED").isPresent());
    assertTrue(StatusType.getByName("ENABLED").isPresent());
  }

  @Test
  public void testGetByNoExistingNameReturnsEmpty()  {
    assertEquals(Optional.empty(), StatusType.getByName("fake123"));
  }
}
