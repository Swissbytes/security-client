package bo.com.ypfbrefinacion.core;

import static org.junit.Assert.assertThat;

import bo.com.ypfbrefinacion.utils.Utils;
import bo.com.ypfbrefinacion.utils.fake.ClientFake;
import bo.com.ypfbrefinacion.utils.fake.LoginFake;
import com.mashape.unirest.http.HttpResponse;
import java.util.List;
import org.apache.http.HttpStatus;
import org.hamcrest.CoreMatchers;
import org.junit.Test;

/**
 * Http Client Test
 *
 * @author David Batista Taboada.
 * @since 1/30/2018
 */
public class HttpClientTest {

  @Test
  public void getOk() throws Exception {
    HttpClient httpClient = new HttpClient(new Configuration("https://reqres.in/api/"));
    HttpResponse<String> httpResponse = httpClient.get("users/1");
    assertThat(httpResponse.getStatus(), CoreMatchers.equalTo(HttpStatus.SC_OK));
  }

  @Test
  public void getNotFound() throws Exception {
    HttpClient httpClient = new HttpClient(new Configuration("https://reqres.in/api/"));
    HttpResponse<String> httpResponse = httpClient.get("unknown/23");
    assertThat(httpResponse.getStatus(), CoreMatchers.equalTo(HttpStatus.SC_NOT_FOUND));
  }

  @Test
  public void postOk() throws Exception {
    HttpClient httpClient = new HttpClient(new Configuration("https://reqres.in/api/"));
    HttpResponse<String> httpResponse = httpClient.post("login", new LoginFake("peter@klaven", "cityslicka"));
    assertThat(httpResponse.getStatus(), CoreMatchers.equalTo(HttpStatus.SC_OK));
  }

  @Test
  public void postBadPost() throws Exception {
    HttpClient httpClient = new HttpClient(new Configuration("https://reqres.in/api/"));
    HttpResponse<String> httpResponse = httpClient.post("login", new LoginFake("peter@klaven", ""));
    assertThat(httpResponse.getStatus(), CoreMatchers.equalTo(HttpStatus.SC_BAD_REQUEST));
  }

  @Test
  public void getTestSerialization() throws Exception {
    HttpClient httpClient = new HttpClient(new Configuration("localhost"));
    ClientFake clientFake = httpClient.parseJsonToObject(
        Utils.fixture("fixtures/user_fake.json"),
        ClientFake.class
    );
    List<ClientFake> postFakes = httpClient.parseJsonToListObject(Utils.fixture
        ("fixtures/list_user_fake.json"), ClientFake.class);
    assertThat(clientFake.id, CoreMatchers.equalTo(1L));
  }

  @Test
  public void getTestListSerialization() throws Exception {
    HttpClient httpClient = new HttpClient(new Configuration("localhost"));
    List<ClientFake> postFakes = httpClient.parseJsonToListObject(Utils.fixture
        ("fixtures/list_user_fake.json"), ClientFake.class);
    assertThat(postFakes.isEmpty(), CoreMatchers.equalTo(false));
  }
}