package bo.com.ypfbrefinacion.filters;

import static org.junit.Assert.assertThat;

import bo.com.ypfbrefinacion.api.enums.SortType;
import bo.com.ypfbrefinacion.api.enums.StatusType;
import java.util.Optional;
import org.hamcrest.CoreMatchers;
import org.junit.Test;

/**
 * Organizational Unit Filter Test.
 *
 * @author David Batista Taboada.
 * @since 2/22/2018
 */
public class OrganizationalUnitFilterTest {

  @Test
  public void getEmptyQueryParams() throws Exception {
    OrganizationalUnitFilter filter = new OrganizationalUnitFilter();
    assertThat(filter.getQueryParams(), CoreMatchers.equalTo(""));
  }

  @Test
  public void getSimpleQueryParams() throws Exception {
    OrganizationalUnitFilter filter = new OrganizationalUnitFilter();
    filter.status = Optional.ofNullable(StatusType.DELETED);
    assertThat(filter.getQueryParams(), CoreMatchers.equalTo("?status=DELETED"));
  }

  @Test
  public void getQueryParamsWithList() throws Exception {
    OrganizationalUnitFilter filter = new OrganizationalUnitFilter();
    filter.initials.add("SYS");
    filter.initials.add("INFRA");
    assertThat(filter.getQueryParams(), CoreMatchers.equalTo("?initials=SYS&initials=INFRA"));
  }

  @Test
  public void getQueryParamsWithListAndPagination() throws Exception {
    OrganizationalUnitFilter filter = new OrganizationalUnitFilter();
    filter.initials.add("SYS");
    filter.initials.add("INFRA");
    filter.perPage = Optional.ofNullable(1L);
    filter.page = Optional.ofNullable(1L);
    filter.fieldToSort = Optional.ofNullable(SortType.ASC);
    assertThat(filter.getQueryParams(), CoreMatchers.equalTo("?page=1&per_page=1&sort=+&initials=SYS&initials=INFRA"));
  }
}