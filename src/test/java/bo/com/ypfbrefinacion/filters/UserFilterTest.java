package bo.com.ypfbrefinacion.filters;

import static org.junit.Assert.assertThat;

import bo.com.ypfbrefinacion.api.enums.StatusType;
import java.util.Optional;
import org.hamcrest.CoreMatchers;
import org.junit.Test;

/**
 * User Filter Test.
 *
 * @author David Batista Taboada.
 * @since 2/23/2018
 */
public class UserFilterTest {
  @Test
  public void getEmptyQueryParams() throws Exception {
    UserFilter filter = new UserFilter();
    assertThat(filter.getQueryParams(), CoreMatchers.equalTo(""));
  }

  @Test
  public void getSimpleQueryParams() throws Exception {
    OrganizationalUnitFilter filter = new OrganizationalUnitFilter();
    filter.status = Optional.of(StatusType.DELETED);
    assertThat(filter.getQueryParams(), CoreMatchers.equalTo("?status=DELETED"));
  }
}
