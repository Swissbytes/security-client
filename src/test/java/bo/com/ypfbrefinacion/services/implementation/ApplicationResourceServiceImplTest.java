package bo.com.ypfbrefinacion.services.implementation;

import bo.com.ypfbrefinacion.services.ApplicationResourceService;
import bo.com.ypfbrefinacion.utils.Utils;
import org.junit.Ignore;
import org.junit.Test;

/**
 * Application Resource Service Implementation Test
 *
 * @author David Batista Taboada.
 * @since 3/28/2018
 */
public class ApplicationResourceServiceImplTest {
  @Test
  @Ignore(value = "This this is only a local enviroment.")
  public void disableApplication() throws Exception {
    ApplicationResourceService applicationResourceService = new ApplicationResourceServiceImpl(Utils.getConfiguration());
    applicationResourceService.disableApplication(20);
  }
}