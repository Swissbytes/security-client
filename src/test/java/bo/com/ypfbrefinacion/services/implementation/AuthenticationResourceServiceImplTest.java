package bo.com.ypfbrefinacion.services.implementation;

import static org.junit.Assert.*;

import bo.com.ypfbrefinacion.api.InternalAccount;
import bo.com.ypfbrefinacion.services.AuthenticationResourceService;
import bo.com.ypfbrefinacion.utils.Utils;
import java.util.Arrays;
import java.util.Optional;
import org.hamcrest.CoreMatchers;
import org.junit.Ignore;
import org.junit.Test;

public class AuthenticationResourceServiceImplTest {

  //Put valid token
  private final String validToken = "eyJhbGciOiJSUzI1NiJ9.eyJpc3MiOiJ3"
      .concat("ZWIiLCJuYmYiOjE1MjUwOTUyNjUsImV4cCI6MTU1NjYzMTI2NSwiaWF0")
      .concat("IjoxNTI1MDk1MjY1LCJhY2NvdW50Ijp7Im5hbWUiOiJHR0wgR2VyZW50")
      .concat("ZSBHZW5lcmFsIiwiaWQiOjExMywidXNlck5hbWUiOiJHR0wuR0VSRU5U")
      .concat("RSJ9LCJqdGkiOiI3NmY2NDliMy1kNGI4LTQ5Y2MtYjFkYy0xYTc5YzZh")
      .concat("MjhiMDMifQ.NKyw3rak7HoKe_jlvcqMvvKOqBD136kK1Yqkr7lCFkWw-")
      .concat("t5ODqKLNqB4WFZ13R2yvxNTTGMK4HHepjaVAt2EUZXgdsnQWmwC2RUDJ")
      .concat("qVRemgcIXKmOk22O8uLd3iOGLkR7wSxPOzAfp3LN4nj0s_X3yznw9paCzbRFv7ylq7IGxc");

  @Test
  @Ignore(value = "This this is only a local enviroment.")
  public void validateToken() throws Exception {
    AuthenticationResourceService authenticationResourceService =
        new AuthenticationResourceServiceImpl(Utils.getConfiguration());
    Optional<InternalAccount> internalAccount = authenticationResourceService.validateToken(validToken);
    assertThat(internalAccount.isPresent(), CoreMatchers.equalTo(true));
  }

  @Test
  @Ignore(value = "This this is only a local enviroment.")
  public void hasPerrmision() throws Exception {
    AuthenticationResourceService authenticationResourceService =
        new AuthenticationResourceServiceImpl(Utils.getConfiguration());
    boolean result = authenticationResourceService.hasPermision(validToken, Arrays.asList("sac.customers.read"));
    assertThat(result, CoreMatchers.equalTo(true));
  }
}