package bo.com.ypfbrefinacion.services.implementation;

import static org.junit.Assert.assertThat;

import bo.com.ypfbrefinacion.api.HealthCheckResponseData;
import bo.com.ypfbrefinacion.services.HealthCheckResourceService;
import bo.com.ypfbrefinacion.utils.Utils;
import java.util.List;
import org.hamcrest.CoreMatchers;
import org.junit.Ignore;
import org.junit.Test;

/**
 * Health Check Service Implementation Test
 *
 * @author David Batista Taboada.
 * @since 4/3/2018
 */
public class HealthCheckResourceServiceImplTest {
  @Test
  @Ignore(value = "This this is only a local enviroment.")
  public void getHealthCheckApplications() throws Exception {
    HealthCheckResourceService healthCheckResourceService = new HealthCheckResourceServiceImpl(Utils.getConfiguration());
    List<HealthCheckResponseData> healthChecks = healthCheckResourceService.getHealthCheckApplications();
    assertThat(healthChecks.isEmpty(), CoreMatchers.equalTo(false));
  }
}