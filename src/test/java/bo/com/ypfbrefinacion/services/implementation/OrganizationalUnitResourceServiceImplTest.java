package bo.com.ypfbrefinacion.services.implementation;

import static org.junit.Assert.assertThat;

import bo.com.ypfbrefinacion.api.OrganizationalUnitData;
import bo.com.ypfbrefinacion.filters.OrganizationalUnitFilter;
import bo.com.ypfbrefinacion.services.OrganizationalUnitResourceService;
import bo.com.ypfbrefinacion.utils.Utils;
import java.util.List;
import org.hamcrest.CoreMatchers;
import org.junit.Ignore;
import org.junit.Test;

/**
 * Organizational Unit Resource Service Implementation Test
 *
 * @author David Batista Taboada.
 * @since 2/22/2018
 */
public class OrganizationalUnitResourceServiceImplTest {
  @Test
  @Ignore(value = "This this is only a local enviroment.")
  public void getAllCurrents() throws Exception {
    OrganizationalUnitResourceService service = new OrganizationalUnitResourceServiceImpl(Utils.getConfiguration());
    List<OrganizationalUnitData> organizationalUnitDatas = service.getCurrents(new OrganizationalUnitFilter());
    assertThat(organizationalUnitDatas.isEmpty(), CoreMatchers.equalTo(false));
  }

  @Test
  @Ignore(value = "This this is only a local enviroment.")
  public void getWithFilterCurrents() throws Exception {
    OrganizationalUnitResourceService service = new OrganizationalUnitResourceServiceImpl(Utils.getConfiguration());
    OrganizationalUnitFilter filter = new OrganizationalUnitFilter();
    filter.initials.add("GGL");
    List<OrganizationalUnitData> organizationalUnitDatas = service.getCurrents(filter);
    assertThat(organizationalUnitDatas.size(), CoreMatchers.equalTo(1));
  }

  @Test
  @Ignore(value = "This this is only a local enviroment.")
  public void getById() throws Exception {
    OrganizationalUnitResourceService service = new OrganizationalUnitResourceServiceImpl(Utils.getConfiguration());
    OrganizationalUnitData ou = service.getById(1L);
    assertThat(ou.id, CoreMatchers.equalTo(1L));
  }
}