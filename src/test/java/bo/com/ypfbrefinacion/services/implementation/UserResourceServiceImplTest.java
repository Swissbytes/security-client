package bo.com.ypfbrefinacion.services.implementation;

import static org.junit.Assert.assertThat;

import bo.com.ypfbrefinacion.api.LoginRequestData;
import bo.com.ypfbrefinacion.api.LoginResponseData;
import bo.com.ypfbrefinacion.api.PaginationResponseData;
import bo.com.ypfbrefinacion.api.UserResponseData;
import bo.com.ypfbrefinacion.core.Configuration;
import bo.com.ypfbrefinacion.filters.UserFilter;
import bo.com.ypfbrefinacion.services.UserResourceService;
import bo.com.ypfbrefinacion.utils.Utils;
import java.util.List;
import org.hamcrest.CoreMatchers;
import org.junit.Ignore;
import org.junit.Test;

/**
 * User Resource Service Implementation Test
 *
 * @author David Batista Taboada.
 * @since 2/1/2018
 */
public class UserResourceServiceImplTest {

  @Test
  @Ignore(value = "This this is only a local enviroment.")
  public void loginTestOk() throws Exception {
    UserResourceService userResourceService = new UserResourceServiceImpl(
        new Configuration("http://localhost:8080/security/rest/"));

    LoginResponseData loginResponseData = userResourceService.login(
        new LoginRequestData("oav2", "miigue-mac", "swissbytes"));

    assertThat(loginResponseData.user.id, CoreMatchers.equalTo(102L));
  }

  @Test
  @Ignore(value = "This this is only a local enviroment.")
  public void getUsersSacSolvers() throws Exception {
    UserResourceService userResourceService = new UserResourceServiceImpl(Utils.getConfiguration());

    PaginationResponseData<UserResponseData> users = userResourceService.getUsersSacSolvers();

    assertThat(users.totalPage, CoreMatchers.equalTo(1L));
  }

  @Test
  @Ignore(value = "This this is only a local enviroment.")
  public void getUsersSacResponsible() throws Exception {
    UserResourceService userResourceService = new UserResourceServiceImpl(Utils.getConfiguration());

    PaginationResponseData<UserResponseData> users = userResourceService.getUsersSacResponsible();

    assertThat(users.totalPage, CoreMatchers.equalTo(1L));
  }

  @Test
  @Ignore(value = "This this is only a local enviroment.")
  public void getUsersSacManagers() throws Exception {
    UserResourceService userResourceService = new UserResourceServiceImpl(Utils.getConfiguration());

    PaginationResponseData<UserResponseData> users = userResourceService.getUsersSacManagers();

    assertThat(users.totalPage, CoreMatchers.equalTo(1L));
  }

  @Test
  @Ignore(value = "This this is only a local enviroment.")
  public void getUsersByFilter() throws Exception {
    UserResourceService userResourceService = new UserResourceServiceImpl(Utils.getConfiguration());
    UserFilter userFilter = new UserFilter();
    userFilter.ids.add(100L);
    userFilter.ids.add(101L);
    List<UserResponseData> users = userResourceService.getUsersByFilter(userFilter);
    assertThat(users.size(), CoreMatchers.equalTo(2));
  }

  @Test
  @Ignore(value = "This this is only a local enviroment.")
  public void getUsersSacList() throws Exception {
    UserResourceService userResourceService = new UserResourceServiceImpl(Utils.getConfiguration());
    List<UserResponseData> users = userResourceService.getSacResponsibleUsers();
    assertThat(users.size(), CoreMatchers.equalTo(2));
  }
}