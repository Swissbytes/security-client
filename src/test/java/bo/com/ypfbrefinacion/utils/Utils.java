package bo.com.ypfbrefinacion.utils;

import bo.com.ypfbrefinacion.core.Configuration;
import bo.com.ypfbrefinacion.exceptions.ConfigurationNotFoundException;
import com.google.common.io.Resources;
import java.io.IOException;
import java.nio.charset.StandardCharsets;

/**
 * Utils
 *
 * @author David Batista Taboada.
 * @since 1/31/2018
 */
public class Utils {

  public static String fixture(String filename){
    try {
      return Resources.toString(Resources.getResource(filename), StandardCharsets.UTF_8).trim();
    } catch (IOException e) {
      throw new IllegalArgumentException(e);
    }
  }

  public static Configuration getConfiguration() throws ConfigurationNotFoundException {
    return new Configuration("http://localhost:8080/security/rest/",
        "65b7c00e20443aa61170d5e7dc9014e0");
  }
}
