package bo.com.ypfbrefinacion.utils.fake;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * Client Fake Example for Test
 *
 * @author David Batista Taboada.
 * @since 1/31/2018
 */
public class ClientFake {
  public long id;
  @JsonProperty(value = "first_name")
  public String firstName;
  @JsonProperty(value = "last_name")
  public String lastName;
  public String avatar;

  public ClientFake() {
  }
}

