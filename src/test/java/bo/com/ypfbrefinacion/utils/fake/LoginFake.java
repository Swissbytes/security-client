package bo.com.ypfbrefinacion.utils.fake;

/**
 * Login Fake Example for Test
 *
 * @author David Batista Taboada.
 * @since 1/31/2018
 */
public class LoginFake {
  public String email;
  public String password;

  public LoginFake(String email, String password) {
    this.email = email;
    this.password = password;
  }
}
